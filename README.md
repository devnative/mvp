# MVP app

This project was bootstrapped with

```sh
npx react-native init mvp --template react-native-template-typescript
```

## Features

- Fetching popular movies from API (themoviedb)
- Search functionality
- Add/remove favorites functionality
- Persist through the app/device the favorites list

## Tech / Packages to be mentioned

- React-Native 0.64.1
- React Navigation v5
- React-Native Config
- React-Redux
- Redux-Saga
- Redux-Persist
- React-Native Async Storage
- Axios
- Typescript

## Installation

You should only need to do in project's main folder:

```sh
npm install
or
yarn
```

After that navigate to /ios folder and do:

```sh
pod install
```

Enjoy!
