import React, {useEffect, useState} from 'react';
import {Text, ImageBackground, FlatList} from 'react-native';
import styles from './styles';

import {useDispatch, useSelector} from 'react-redux';
import {getMovies, getSearchMovies} from '../../redux/actions/moviesActions';
import {addToFavs, removeFavs} from '../../redux/actions/favoritesActions';
import HorizontalCard from '../../components/HorizontalCard/HorizontalCard';
import Searchbar from '../../components/SearchBar/Searchbar';
import DashboarSkeleton from '../../components/DashboardSkeleton/DashboardSkeleton';
import Images from '../../themes/Images';
import {IFavsState} from '../../models/favorites';
import {IMoviesState} from '../../models/movies';
import {IMovieItemType} from '../../models/movieItemType';
interface IState {
  moviesReducer: IMoviesState;
}
interface IState {
  favoritesReducer: IFavsState;
}

const Dashboard: React.FC = () => {
  const dispatch = useDispatch();
  const moviesSelector = useSelector(
    (state: IState) => state.moviesReducer.movies?.results,
  );
  const moviesFetching = useSelector(
    (state: IState) => state.moviesReducer.movies?.fetching,
  );
  const favsSelector = useSelector(
    (state: IState) => state.favoritesReducer.favorites,
  );
  const [value, setValue] = useState();

  function updateSearch(searchedValue: string) {
    if (searchedValue.length >= 3) {
      dispatch(getSearchMovies(searchedValue));
    }
  }

  function onPressFavs(item: IMovieItemType) {
    if (favsSelector.length === 0) {
      dispatch(addToFavs(item));
    }
    favsSelector.map(fav => {
      if (fav.id === item.id) {
        dispatch(removeFavs(item.id));
      } else {
        dispatch(addToFavs(item));
      }
    });
  }

  useEffect(() => {
    dispatch(getMovies());
  }, [dispatch]);

  const renderItem = ({item}: {item: IMovieItemType}) => {
    return <HorizontalCard item={item} onPressFavs={onPressFavs} />;
  };

  if (moviesFetching) {
    return <DashboarSkeleton />;
  }

  return (
    <ImageBackground source={Images.background} style={styles.bgImage}>
      <Searchbar
        value={value}
        updateSearch={updateSearch}
        style={styles.searchBar}
      />
      <Text style={styles.title}>Popular Movies</Text>
      <FlatList
        data={moviesSelector}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        horizontal={true}
      />
    </ImageBackground>
  );
};

export default Dashboard;
