import React from 'react';
import {Text, ImageBackground, FlatList} from 'react-native';
import styles from './styles';

import {useDispatch, useSelector} from 'react-redux';
import {addToFavs, removeFavs} from '../../redux/actions/favoritesActions';
import VerticalCard from '../../components/VerticalCard/VerticalCard';
import Images from '../../themes/Images';
import {IFavsState} from '../../models/favorites';
import {IMovieItemType} from '../../models/movieItemType';

interface IState {
  favoritesReducer: IFavsState;
}

const Favorites: React.FC = () => {
  const dispatch = useDispatch();
  const favsSelector = useSelector(
    (state: IState) => state.favoritesReducer.favorites,
  );

  function onPressFavs(item: IMovieItemType) {
    if (favsSelector.length === 0) {
      dispatch(addToFavs(item));
    }
    favsSelector.map(fav => {
      if (fav.id === item.id) {
        dispatch(removeFavs(item.id));
      } else {
        dispatch(addToFavs(item));
      }
    });
  }

  const renderItem = ({item}: {item: IMovieItemType}) => {
    return <VerticalCard item={item} onPressFavs={onPressFavs} />;
  };

  return (
    <ImageBackground source={Images.background} style={styles.bgImage}>
      <Text style={styles.title}>Favorite Movies</Text>
      <FlatList
        data={favsSelector}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
    </ImageBackground>
  );
};

export default Favorites;
