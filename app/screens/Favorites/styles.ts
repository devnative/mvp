import {StyleSheet} from 'react-native';
import Colors from '../../themes/Colors';
import Sizes from '../../themes/Sizes';

export default StyleSheet.create({
  mainContainer: {
    padding: Sizes.pad10 * 2,
    backgroundColor: Colors.transparent,
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    color: Colors.white,
    fontSize: 16,
    textTransform: 'uppercase',
    fontWeight: '300',
    letterSpacing: 1,
    marginLeft: Sizes.pad10 * 2,
    marginBottom: Sizes.gutterSize * 2,
  },
  bgImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    paddingTop: 100,
  },
});
