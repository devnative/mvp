import {
  FETCHING_REQUEST,
  FETCHING_SUCCESS,
  FETCHING_FAILURE,
  FETCHING_SEARCH_REQUEST,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
} from '../actions/types';

export const getMovies = () => ({
  type: FETCHING_REQUEST,
});

export const setMovies = movies => ({
  type: FETCHING_SUCCESS,
  payload: movies,
});

export const getMoviesError = movies => ({
  type: FETCHING_FAILURE,
});

export const getSearchMovies = term => ({
  type: FETCHING_SEARCH_REQUEST,
  payload: term,
});

export const setSearchMovies = movies => ({
  type: FETCHING_SEARCH_SUCCESS,
  payload: movies,
});

export const getSearchMoviesError = () => ({
  type: FETCHING_SEARCH_FAILURE,
});
