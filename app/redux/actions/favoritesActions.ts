import {SET_FAVORITES, REMOVE_FAVORITES} from './types';

export const addToFavs = favs => ({
  type: SET_FAVORITES,
  payload: favs,
});

export const removeFavs = favs => ({
  type: REMOVE_FAVORITES,
  payload: favs,
});
