import {
  FETCHING_FAILURE,
  FETCHING_REQUEST,
  FETCHING_SUCCESS,
  FETCHING_SEARCH_REQUEST,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
} from '../actions/types';

const initialState: State = {
  fetching: false,
  movies: [],
};

const moviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_REQUEST:
      return {
        ...state,
        fetching: true,
        movies: action.payload,
      };
    case FETCHING_SUCCESS:
      return {
        ...state,
        fetching: false,
        movies: action.payload,
      };
    case FETCHING_FAILURE:
      return {
        ...state,
        fetching: false,
        movies: [],
      };
    case FETCHING_SEARCH_REQUEST:
      return {
        ...state,
        fetching: true,
        movies: action.payload,
      };
    case FETCHING_SEARCH_SUCCESS:
      return {
        ...state,
        fetching: false,
        movies: action.payload,
      };
    case FETCHING_SEARCH_FAILURE:
      return {
        ...state,
        fetching: false,
        movies: [],
      };
    default:
      return state;
  }
};

export default moviesReducer;
