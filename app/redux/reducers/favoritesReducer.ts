import {SET_FAVORITES, REMOVE_FAVORITES} from '../actions/types';

const initialState: State = {
  fetching: false,
  favorites: [],
};

const favoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FAVORITES:
      return {
        ...state,
        fetching: true,
        favorites: [...state.favorites, action.payload],
      };
    case REMOVE_FAVORITES:
      return {
        ...state,
        fetching: false,
        favorites: state.favorites.filter(item => item.id !== action.payload),
      };
    default:
      return state;
  }
};

export default favoritesReducer;
