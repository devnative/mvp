import {all} from 'redux-saga/effects';
import {watchFetchMovies, watchSearchMovies} from './moviesSaga';

export default function* rootSaga() {
  yield all([watchFetchMovies(), watchSearchMovies()]);
}
