import {call, put, takeEvery} from 'redux-saga/effects';
import {
  setMovies,
  getMoviesError,
  setSearchMovies,
  getSearchMoviesError,
} from '../redux/actions/moviesActions';
import {
  FETCHING_REQUEST,
  FETCHING_SEARCH_REQUEST,
} from '../redux/actions/types';
import {getMoviesApi, searchMoviesApi} from '../services/moviesService';

export function* handleFetchMovies(action) {
  try {
    const movies = yield call(getMoviesApi);
    yield put(setMovies(movies));
  } catch (error) {
    console.log(error);
    yield put(getMoviesError);
  }
}

export function* watchFetchMovies() {
  yield takeEvery(FETCHING_REQUEST, handleFetchMovies);
}

export function* handleSearchMovies(action) {
  try {
    const movies = yield call(searchMoviesApi, action.payload);
    yield put(setSearchMovies(movies));
  } catch (error) {
    console.log(error);
    yield put(getSearchMoviesError);
  }
}

export function* watchSearchMovies() {
  yield takeEvery(FETCHING_SEARCH_REQUEST, handleSearchMovies);
}
