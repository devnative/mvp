import Config from 'react-native-config';

const baseUrl = Config.BASE_URL;
const apiKey = Config.API_KEY;
const imagesUrl = Config.IMAGES_URL;

// const ENVIRONMENT = {
//   PROD: 'PROD',
//   DEV: 'DEV',
// };

// const currentEnv = ENVIRONMENT.DEV;

const popularMoviesUrl = baseUrl + '3/movie/popular?api_key=' + apiKey;
const upComingMoviesUrl = baseUrl + '3/movie/upcoming?api_key=' + apiKey;
const searchUrl =
  baseUrl +
  '3/search/movie?api_key=' +
  apiKey +
  '&language=en-US&page=1&include_adult=false&query=';

let ApiConfig = {
  upComingMoviesUrl,
  imagesUrl,
  popularMoviesUrl,
  searchUrl,
};

export {ApiConfig};
