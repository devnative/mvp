import {ApiConfig} from '../services/apiConfig';
import axios from 'axios';

export const getMoviesApi = async () => {
  const response = await axios.get(`${ApiConfig.popularMoviesUrl}`);
  return response.data;
};

export const searchMoviesApi = async searchKeyWord => {
  const response = await axios.get(`${ApiConfig.searchUrl} + ${searchKeyWord}`);
  return response.data;
};
