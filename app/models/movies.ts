export interface IMoviesState {
  movies: {results: []; fetching: boolean};
}
