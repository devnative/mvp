import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Dasbhoard from '../screens/Dashboard/Dashboard';
import Favorites from '../screens/Favorites/Favorites';
const Tab = createBottomTabNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: 'yellow',
          inactiveTintColor: 'lightgray',
          activeBackgroundColor: '#rgba(0,0,0,0.1)',
          inactiveBackgroundColor: '#rgba(0,0,0,0.1)',
          style: {
            backgroundColor: 'rgba(0,0,0,0.1)',
            paddingBottom: 3,
            position: 'absolute',
          },
          labelStyle: {
            textTransform: 'uppercase',
            fontSize: 12,
            marginBottom: 8,
          },
        }}>
        <Tab.Screen
          name="Movies"
          component={Dasbhoard}
          options={{
            tabBarLabel: 'Dashboard',
            tabBarIcon: ({focused, color, size}) => (
              <MaterialIcons
                name="home"
                color={focused ? 'yellow' : '#fff'}
                size={32}
                focused={focused}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Favorites"
          component={Favorites}
          options={{
            tabBarLabel: 'Favorites',
            tabBarIcon: ({focused, color, size}) => (
              <MaterialIcons
                name="favorite"
                color={focused ? 'yellow' : '#fff'}
                size={32}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
