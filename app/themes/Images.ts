const images = {
  background: require('../images/bg2.jpg'),
  star: require('../images/star.png'),
  emptyHeart: require('../images/heart.png'),
  coloredHeart: require('../images/lover.png'),
  search: require('../images/search.png'),
  close: require('../images/clear.png'),
};

export default images;
