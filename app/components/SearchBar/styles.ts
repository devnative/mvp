import {StyleSheet} from 'react-native';
import Colors from '../../themes/Colors';
import Sizes from '../../themes/Sizes';

export default StyleSheet.create({
  txtError: {
    marginTop: '2%',
    width: '89%',
    color: Colors.white,
  },
  vwClear: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    fontSize: Sizes.pad10 * 2,
    flex: 1,
    color: Colors.primary,
  },

  vwSearch: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icSearch: {
    height: 18,
    width: 18,
  },
  searchContainer: {
    backgroundColor: Colors.primaryDark,
    width: '90%',
    height: 40,
    flexDirection: 'row',
    borderRadius: Sizes.gutterSize,
  },
  container: {
    height: 72,
    alignItems: 'center',
  },
});
