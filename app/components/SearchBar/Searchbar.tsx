import React, {useState} from 'react';
import {View, Text, TouchableOpacity, TextInput, Image} from 'react-native';
import {useDispatch} from 'react-redux';
import {getMovies} from '../../redux/actions/moviesActions';
import Images from '../../themes/Images';
import styles from './styles';

type OwnProps = {
  value: String;
  updateSearch: Function;
  style: {};
};

const Searchbar: React.FC<OwnProps> = ({value, updateSearch, style}) => {
  const [query, setQuery] = useState();
  const [error, setError] = useState();
  const dispatch = useDispatch();
  const onClear = () => {
    setQuery('');
    dispatch(getMovies());
  };
  return (
    <View style={[styles.container, style]}>
      <View style={styles.searchContainer}>
        <View style={styles.vwSearch}>
          <Image style={styles.icSearch} source={Images.search} />
        </View>

        <TextInput
          value={query}
          placeholder="Search"
          placeholderTextColor="#466399"
          style={styles.textInput}
          onChangeText={text => {
            var letters = /^$|^[a-zA-Z._\b ]+$/;
            if (text.length > 12) {
              setError('Query too long.');
            } else if (text.match(letters)) {
              setQuery(text);
              updateSearch(text);
              if (error) {
                setError(false);
              }
            } else {
              setError('Please only enter alphabets');
            }
          }}
        />
        {query ? (
          <TouchableOpacity onPress={() => onClear()} style={styles.vwClear}>
            <Image style={styles.icSearch} source={Images.close} />
          </TouchableOpacity>
        ) : (
          <View style={styles.vwClear} />
        )}
      </View>
      {error && <Text style={styles.txtError}>{error}</Text>}
    </View>
  );
};
export default Searchbar;
