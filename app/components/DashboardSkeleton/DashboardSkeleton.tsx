import * as React from 'react';
import {ImageBackground, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import styles from './styles';
import Images from '../../themes/Images';

const cardLoop = [1, 2, 3];

const DashboardSkeleton: React.FC = () => {
  return (
    <ImageBackground source={Images.background} style={styles.bgImage}>
      <SkeletonPlaceholder>
        <View style={styles.mainContainer}>
          <View style={styles.rowContainer}>
            <View style={styles.searchBar} />
          </View>
          <View style={styles.rowContainer}>
            {cardLoop.map(() => (
              <View style={styles.cardContainer}>
                <View style={styles.card} />
                <View style={styles.textRow} />
                <View style={styles.dateRow} />
                <View style={styles.ratingContainer}>
                  <View style={styles.rating} />
                  <View style={styles.rating} />
                  <View style={styles.rating} />
                  <View style={styles.rating} />
                  <View style={styles.rating} />
                </View>
              </View>
            ))}
          </View>
        </View>
      </SkeletonPlaceholder>
    </ImageBackground>
  );
};

export default DashboardSkeleton;
