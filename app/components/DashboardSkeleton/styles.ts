import {StyleSheet} from 'react-native';
import Sizes from '../../themes/Sizes';

export default StyleSheet.create({
  bgImage: {
    flex: 1,
    resizeMode: 'cover',
    paddingTop: 80,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: Sizes.pad10 * 2,
  },
  searchBar: {
    width: Sizes.screen.width * 0.9,
    height: 40,
    borderRadius: Sizes.gutterSize / 2,
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 1,
  },
  cardContainer: {
    flexDirection: 'column',
  },
  card: {
    width: 160,
    height: 200,
    marginRight: Sizes.pad10 * 2,
    marginTop: 80,
    borderRadius: Sizes.gutterSize,
  },
  textRow: {
    width: 160,
    height: 14,
    borderRadius: Sizes.gutterSize / 2,
    marginTop: 300,
  },
  dateRow: {
    width: 120,
    height: 14,
    borderRadius: Sizes.gutterSize / 2,
    marginTop: 320,
  },
  ratingContainer: {
    flexDirection: 'row',
    marginTop: 340,
  },
  rating: {
    width: 14,
    height: 14,
    borderRadius: 7,
    marginRight: 2,
  },
});
