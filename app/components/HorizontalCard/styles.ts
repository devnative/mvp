import {StyleSheet} from 'react-native';
import Colors from '../../themes/Colors';
import Sizes from '../../themes/Sizes';

export default StyleSheet.create({
  container: {
    width: 160,
    height: 240,
    marginLeft: Sizes.pad10 * 2,
  },
  bgImageStyle: {
    borderRadius: Sizes.gutterSize * 2,
  },
  title: {
    fontSize: Sizes.font14,
    color: Colors.white,
    textTransform: 'uppercase',
    marginTop: Sizes.pad10 * 2,
    marginLeft: Sizes.pad10 * 2,
  },
  releaseDate: {
    fontSize: Sizes.font12,
    color: Colors.beige,
    marginTop: Sizes.gutterSize,
    marginLeft: Sizes.pad10 * 2,
  },
  underPosterContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    maxWidth: 160,
  },
  starContainer: {
    marginTop: Sizes.gutterSize,
    marginLeft: -Sizes.gutterSize * 3,
  },
  favContainer: {
    position: 'absolute',
    bottom: Sizes.pad10,
    right: Sizes.pad10,
  },
  favImage: {
    width: Sizes.gutterSize * 3,
    height: Sizes.gutterSize * 3,
  },
  notFavImage: {
    width: Sizes.gutterSize * 3,
    height: Sizes.gutterSize * 3,
    tintColor: Colors.white,
  },
});
