import * as React from 'react';
import {
  ImageBackground,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {ApiConfig} from '../../services/apiConfig';
import {Rating} from 'react-native-ratings';
import {useSelector} from 'react-redux';
import Images from '../../themes/Images';
import styles from './styles';
import {IMovieItemType} from '../../models/movieItemType';
import {IFavsState} from '../../models/favorites';

type OwnProps = {
  item: IMovieItemType;
  onPressFavs: Function;
};

interface IState {
  favoritesReducer: IFavsState;
}

const HorizontalCard: React.FC<OwnProps> = ({item, onPressFavs}) => {
  const imgSource = ApiConfig.imagesUrl + item.poster_path;
  const favsSelector = useSelector(
    (state: IState) => state.favoritesReducer.favorites,
  );
  const checkFav = favsSelector.filter(fav => {
    if (fav.id === item.id) {
      return true;
    } else {
      return false;
    }
  });
  return (
    <View>
      <ImageBackground
        source={{uri: imgSource}}
        style={styles.container}
        imageStyle={styles.bgImageStyle}>
        <View style={styles.favContainer}>
          <TouchableOpacity onPress={() => onPressFavs(item)}>
            <Image
              source={
                checkFav.length > 0 ? Images.coloredHeart : Images.emptyHeart
              }
              style={checkFav.length > 0 ? styles.favImage : styles.notFavImage}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={styles.underPosterContainer}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.releaseDate}>{item.release_date}</Text>
        <View style={styles.starContainer}>
          <Rating
            ratingCount={5}
            imageSize={20}
            tintColor={'#b7b100'}
            startingValue={item.vote_average}
            readonly={true}
            type="custom"
            ratingImage={Images.star}
            ratingColor="transparent"
          />
        </View>
      </View>
    </View>
  );
};

export default HorizontalCard;
